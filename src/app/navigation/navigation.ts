export const navigation = [
    {
        'id'      : 'applications',
        'title'   : 'Cosas',
        'type'    : 'group',
        'children': [
            {
                'id'   : 'sample',
                'title': 'Mapa',
                'translate': 'Mapa',
                'type' : 'item',
                'icon' : 'place',
                'url'  : '/sample'
                
            },
            {
                'id'   : 'fixture',
                'title': 'Fixture',
                'translate': 'Fixture',
                'type' : 'item',
                'icon' : 'merge_type',
                'url'  : '/fixture'
                
            }
        ]
    }
];
