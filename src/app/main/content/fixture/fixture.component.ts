import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { AfterViewInit, Component, ElementRef, ViewChild } from "@angular/core";

// This is necessary to access ol3!

@Component({
    selector: 'my-app',
    templateUrl: './fixture.component.html',
    styleUrls  : ['./fixture.component.scss'],
    // The "#" (template reference variable) matters to access the map element with the ViewChild decorator!
})

export class FuseFixtureComponent implements AfterViewInit {
    // This is necessary to access the html element to set the map target (after view init)!
    // After view init the map target can be set!
    ngAfterViewInit() {
    }
}