import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';

import { FuseSharedModule } from '@fuse/shared.module';

import { FuseFixtureComponent } from './fixture.component';

const routes = [
    {
        path     : 'fixture',
        component: FuseFixtureComponent
    }
];

@NgModule({
    declarations: [
        FuseFixtureComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        TranslateModule,

        FuseSharedModule
    ],
    exports     : [
        FuseFixtureComponent
    ]
})

export class FuseFixtureModule
{
}
