import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';
import { AfterViewInit, Component, ElementRef, ViewChild } from "@angular/core";

// This is necessary to access ol3!
declare var ol: any;

@Component({
    selector: 'my-app',
    // template: `
    // <div #mapElement id="map" class="map"> </div> 
    // `,
    templateUrl: './sample.component.html',
    styleUrls  : ['./sample.component.scss'],
    // The "#" (template reference variable) matters to access the map element with the ViewChild decorator!
})

export class FuseSampleComponent implements AfterViewInit {
    // This is necessary to access the html element to set the map target (after view init)!
    @ViewChild("mapElement") mapElement: ElementRef;

    public map: any;

    constructor(){
        var osm_layer: any = new ol.layer.Tile({
            source: new ol.source.OSM()
        });

        var theurl = 'http://104.236.111.134:8080/geoserver/sige/wms';

        // note that the target cannot be set here!
        this.map = new ol.Map({
            layers: [osm_layer],
            view: new ol.View({
              center: [	-6250000 ,-4150000. ],
                  zoom: 15
            })
        });

        //  add the bk79 layer. All other wms layers will following this pattern, but with a twist
    var caminos: any = new ol.layer.Image({
          source: new ol.source.ImageWMS({
            url: theurl,
            params: {'LAYERS': 'sige:planet_osm_roads'},
            serverType: 'geoserver'
          })    
        });
    caminos.setOpacity(0);
    this.map.addLayer(caminos);
}


    // After view init the map target can be set!
    ngAfterViewInit() {
        this.map.setTarget(this.mapElement.nativeElement.id);
    }
}